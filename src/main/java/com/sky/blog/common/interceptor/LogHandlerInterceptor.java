package com.sky.blog.common.interceptor;

import com.sky.blog.user.entity.User;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class LogHandlerInterceptor implements HandlerInterceptor {
    /**
     * controller 执行之前调用
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        String url = request.getRequestURI();
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("userinfo");
//        if(user!=null){
//            return true;
//        }
        if (url.indexOf("admin")>=0){
            if(user!=null||url.equals("/admin/logout")||url.equals("/admin/undefined")){
                return true;
            }
//            response.sendRedirect("index/login");
//            request.getRequestDispatcher("index/login").forward(request,response);
            return false;
        }
//        if (url.equals("/")){
//        request.getRequestDispatcher("index/index").forward(request,response);
//        return true;
//        }
        return true;
    }

    /**
     * controller 执行之后，且页面渲染之前调用
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {



    }

    /**
     * 页面渲染之后调用，一般用于资源清理操作
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {

    }
}
