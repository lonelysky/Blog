package com.sky.blog.common.util;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.model.PutObjectResult;
import com.qcloud.cos.region.Region;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * 腾讯云存储
 */
@Component
public class cloudStorageImpl implements  cloudStorage {




    @Override
    public String upLoad(File file) {
        /**
         * 腾讯云配置信息start
         */
        // 1 初始化用户身份信息(secretId, secretKey)
        COSCredentials cred = new BasicCOSCredentials("AKIDthLrU3mpwuhJl5dZdVCjAPYWL0yt3jfQ", "kJn6IKBrMYihUAvY5SfhaJsGTVM2Xr1z");
        // 2 设置bucket的区域, COS地域的简称请参照 https://cloud.tencent.com/document/product/436/6224
        ClientConfig clientConfig = new ClientConfig(new Region("ap-chengdu"));
        // 3 生成cos客户端
        COSClient cosClient = new COSClient(cred, clientConfig);
        // bucket的命名规则为{name}-{appid} ，此处填写的存储桶名称必须为此格式
        String bucketName = "pic-1252705305";
        // 简单文件上传, 最大支持 5 GB, 适用于小文件上传, 建议 20 M 以下的文件使用该接口
        // 大文件上传请参照 API 文档高级 API 上传
//        File localFile = new File("src/test/resources/len5M.txt");
        File localFile = file;
        // 指定要上传到 COS 上的路径
        String key = "/imgs/"+file.getName();
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, key, localFile);
        PutObjectResult putObjectResult = cosClient.putObject(putObjectRequest);
        cosClient.shutdown();
        /**
         * 腾讯云配置信息end
         */
        String url = "https://pic-1252705305.cos.ap-chengdu.myqcloud.com/imgs/"+file.getName()+"";
        return url;
    }

//    public static void main(String args[]){
//        cloudStorageImpl c = new cloudStorageImpl();
//        File file = new File("E:/sss.jpg");
//        String url = c.upLoad(file);
//        System.out.println(url);
//    }
}
