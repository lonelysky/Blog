package com.sky.blog.common.vo;


import com.sky.blog.common.page.PageQueryBean;

/**
 * 扩展
 */
public class QueryCondition  extends PageQueryBean {

    private String name;

    private String user_img;

    private String user_sign;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser_img() {
        return user_img;
    }

    public void setUser_img(String user_img) {
        this.user_img = user_img;
    }

    public String getUser_sign() {
        return user_sign;
    }

    public void setUser_sign(String user_sign) {
        this.user_sign = user_sign;
    }
}
