package com.sky.blog.common.constants;

public class Constants {
    public static final int RESP_STATUS_OK = 200;

    public static final int RESP_STATUS_NOAUTH = 401;

    public static final int RESP_STATUS_INTERNAL_ERROR = 500;

    public static final int RESP_STATUS_BADREQUEST = 400;



    /**
     * 自定义的常量，自己玩瞎弄的
     */
    public static  int POST_LIST = 3;
    public static  Integer USER_ID = 1 ;
    public static  int NEW_POST_NUMBER = 5;
    public static  int SM_LIST = 20;
    public static  int admin_list = 15;
    public static  String img = "http://172.22.16.108/images/6.jpg";

}
