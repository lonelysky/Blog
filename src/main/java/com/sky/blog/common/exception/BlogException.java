package com.sky.blog.common.exception;

import com.sky.blog.common.constants.Constants;

public class BlogException extends Exception{
    public BlogException(String message){
        super(message);
    }
    public int getStatusCode(){
        return Constants.RESP_STATUS_INTERNAL_ERROR;
    }
}
