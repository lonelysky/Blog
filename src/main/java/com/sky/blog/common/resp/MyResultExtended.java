package com.sky.blog.common.resp;

/**
 * 扩展类
 */
public class MyResultExtended<T> extends MyResult {
    private String img;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
