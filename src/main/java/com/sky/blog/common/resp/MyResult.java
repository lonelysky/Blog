package com.sky.blog.common.resp;

import com.sky.blog.common.constants.Constants;


public class MyResult<T> {
    //状态码
    private int code = Constants.RESP_STATUS_OK;

    private String message;

    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
