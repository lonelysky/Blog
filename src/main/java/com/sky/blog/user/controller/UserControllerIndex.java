package com.sky.blog.user.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class UserControllerIndex {
    @RequestMapping("/skylogin")
    public String ts(){
        return "admin/login";
    }

//    @RequestMapping("/adminindex")
//    public String t(){
//        return "admin/index";
//    }

    @RequestMapping("index/index")
    public String s(){
        return "front/index";
    }
    @RequestMapping("/404")
    public String error(){
        return "front/404";
    }

}
