package com.sky.blog.user.controller;

import com.sky.blog.common.constants.Constants;
import com.sky.blog.common.exception.BlogException;
import com.sky.blog.common.resp.MyResult;
import com.sky.blog.common.util.Log;
import com.sky.blog.links.entity.Link;
import com.sky.blog.user.dao.UserMapper;
import com.sky.blog.user.entity.User;
import com.sky.blog.user.service.UserService;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserService userService;

    @RequestMapping("/login")
    public String login(HttpServletRequest request,String username, String password){
        try {
            boolean flag = userService.checkUser(request,username,password);
            if (flag){
                return "admin/index";
            }else{
                throw new BlogException("密码错误，开始计准备拒绝链接");
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (BlogException e) {
            e.printStackTrace();
        }
        return null;
    }

    @RequestMapping("/loaduser")
    @ResponseBody
    public MyResult<User> loadUser(){
        try {
            MyResult<User> result = new MyResult<>();
            User user = userMapper.selectByPrimaryKey(Constants.USER_ID);
            result.setData(user);
            return result;
        } catch (Exception e) {
            Log.log.error("Failed to load user",e);
        }
        return null;
    }

    @RequestMapping("/links")
    @ResponseBody
    public MyResult<List<Link>> initLins(){
        try {
            MyResult<List<Link>> result = new MyResult<>();
            List<Link> list = userService.listLinks();
            result.setData(list);
            return result;
        }catch (Exception e){
            Log.log.error("Failed to load link",e);
        }
        return null;
    }


    /*@RequestMapping("/test")
    public String Test(){
        return "success";
    }*/
}
