package com.sky.blog.user.service;

import com.sky.blog.common.util.MD5Utils;
import com.sky.blog.links.dao.LinkMapper;
import com.sky.blog.links.entity.Link;
import com.sky.blog.user.dao.UserMapper;
import com.sky.blog.user.entity.User;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private LinkMapper linkMapper;
    @Autowired
    private UserMapper userMapper;
    @Override
    public List<Link> listLinks() {
        List<Link> list = linkMapper.findAll();
        return list;
    }

    @Override
    public boolean checkUser(HttpServletRequest request,String username,String password) throws UnsupportedEncodingException, NoSuchAlgorithmException {

        //查数据库 如果查到数据  调用MD5加密对比密码
        User user = userMapper.selectByUserName(username);
        if(user!=null){
            if(MD5Utils.checkPassword(password,user.getPassword())){
                //校验成功  设置session
                request.getSession().setAttribute("userinfo",user);
                return true;
            }else{
                // 校验失败  返回校验失败signal
                return false;
            }
        }else {
            // 校验失败  返回校验失败signal
           return false;
        }
    }
}
