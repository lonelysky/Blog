package com.sky.blog.user.service;

import com.sky.blog.links.entity.Link;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface UserService {
    List<Link> listLinks();

    public boolean checkUser(HttpServletRequest request, String username, String password) throws UnsupportedEncodingException, NoSuchAlgorithmException;
}
