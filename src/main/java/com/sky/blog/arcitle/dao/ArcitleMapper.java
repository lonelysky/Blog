package com.sky.blog.arcitle.dao;

import com.sky.blog.arcitle.entity.Arcitle;
import com.sky.blog.common.page.PageQueryBean;
import com.sky.blog.common.vo.QueryCondition;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ArcitleMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Arcitle record);

    int insertSelective(Arcitle record);

    Arcitle selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Arcitle record);

    int updateByPrimaryKeyWithBLOBs(Arcitle record);

    int updateByPrimaryKey(Arcitle record);

    int selectByCondition();

    List<Arcitle> selectArcitlePage(PageQueryBean condition);

    List<Arcitle> selectNewPost(int i);

    List<Arcitle> selectSmpage(QueryCondition condition);
}