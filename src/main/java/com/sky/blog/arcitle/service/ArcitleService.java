package com.sky.blog.arcitle.service;

import com.sky.blog.arcitle.entity.Arcitle;
import com.sky.blog.common.exception.BlogException;
import com.sky.blog.common.page.PageQueryBean;
import com.sky.blog.common.vo.QueryCondition;

import java.util.List;

public interface ArcitleService {

    QueryCondition listAttend(QueryCondition condition) throws BlogException;

    Arcitle getPost(int id);

    List<Arcitle> getNewPost();


    QueryCondition smList(QueryCondition condition) throws BlogException;
}
