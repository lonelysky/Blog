package com.sky.blog.arcitle.service;

import com.sky.blog.arcitle.dao.ArcitleMapper;
import com.sky.blog.arcitle.entity.Arcitle;
import com.sky.blog.common.constants.Constants;
import com.sky.blog.common.exception.BlogException;
import com.sky.blog.common.util.Log;
import com.sky.blog.common.vo.QueryCondition;
import com.sky.blog.user.dao.UserMapper;
import com.sky.blog.user.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class ArcitleServiceImpl implements ArcitleService {

    @Autowired
    private ArcitleMapper arcitleMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public QueryCondition listAttend(QueryCondition condition) throws BlogException {
        try {
            int count = arcitleMapper.selectByCondition();
            if (count==0){
                throw new BlogException("查询失败");
            }
            QueryCondition pageResult = new QueryCondition();
            //总条数
            pageResult.setTotalRows(count);
            //当前页
            pageResult.setCurrentPage(condition.getCurrentPage());
            //总页数
            pageResult.setPageSize(condition.getPageSize());
            //将数据加入
            List<Arcitle> arcitleList = arcitleMapper.selectArcitlePage(condition);
            pageResult.setItems(arcitleList);
            //添加扩展条件
//            User user = userMapper.selectByPrimaryKey(Constants.USER_ID);
//            String name = user.getUsername();
//            pageResult.setName(name);
            //头像
//            String imgUrl = user.getImagename();
//            pageResult.setUser_img(imgUrl);
            //座右铭
//            String sign = user.getSign();
//            pageResult.setUser_sign(sign);
            return pageResult;
        } catch (Exception e) {
            Log.log.error("Fail select to page",e);
        }
        return null;
    }

    @Override
    public Arcitle getPost(int id) {
        Arcitle arcitle = arcitleMapper.selectByPrimaryKey(id);
        return arcitle;
    }

    @Override
    public List<Arcitle> getNewPost() {
        //挂最近5篇
        List<Arcitle> list = new ArrayList<>();
        //查询最新的 i 条 后期可通过更改Constans 的常量值更改数量
        list = arcitleMapper.selectNewPost(Constants.NEW_POST_NUMBER);
        return list;
    }

    @Override
    public QueryCondition smList(QueryCondition condition) throws BlogException {

        try {
            int count = arcitleMapper.selectByCondition();
            QueryCondition result = new QueryCondition();
            //总条数
            result.setTotalRows(count);
            //当前页
            result.setCurrentPage(condition.getCurrentPage());
            //总页数
            result.setPageSize(condition.getPageSize());
            //数据包装
            List<Arcitle> list = arcitleMapper.selectSmpage(condition);
            result.setItems(list);
            int topage = 0;
            int i = count%Constants.SM_LIST;
            int j = count/Constants.SM_LIST;
            if (i==0){
                topage = i;
            }else{
                topage = j+1;
            }
            result.setTotalPage(topage);
            return result;
        } catch (Exception e) {
            Log.log.error("Failed to load article list",e);
            throw new BlogException("加载文章列表失败");
        }
    }


}
