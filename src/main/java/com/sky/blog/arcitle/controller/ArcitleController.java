package com.sky.blog.arcitle.controller;

import com.sky.blog.arcitle.entity.Arcitle;
import com.sky.blog.arcitle.service.ArcitleService;
import com.sky.blog.common.constants.Constants;
import com.sky.blog.common.exception.BlogException;
import com.sky.blog.common.page.PageQueryBean;
import com.sky.blog.common.resp.MyResult;
import com.sky.blog.common.util.Log;
import com.sky.blog.common.vo.QueryCondition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("post")
public class ArcitleController {

    @Autowired
    private ArcitleService arcitleService;

    @RequestMapping("/list")
    @ResponseBody
    public QueryCondition listArcitle(QueryCondition queryCondition) throws BlogException {
        queryCondition.setDefaultPageSize(Constants.POST_LIST);
        QueryCondition page = arcitleService.listAttend(queryCondition);

        return page;
    }

    @RequestMapping("/post/{id}")
    @ResponseBody
    public MyResult<Arcitle> post(@PathVariable int id){

        MyResult<Arcitle> result = null;
        try {
            result = new MyResult<Arcitle>();
            Arcitle arcitle = arcitleService.getPost(id);
            result.setData(arcitle);
            return result;
        } catch (Exception e) {
            result.setCode(Constants.RESP_STATUS_INTERNAL_ERROR);
            Log.log.error("Fail to get post",e);
        }
        return null;
    }

    @RequestMapping("/newpost")
    @ResponseBody
    public MyResult<List<Arcitle>> newPost(){
        MyResult<List<Arcitle>> result = null;
        List<Arcitle> list = null;
        try {
             result = new MyResult<>();
             list = arcitleService.getNewPost();
             result.setData(list);
             return result;
        }catch (Exception e){
            result.setCode(Constants.RESP_STATUS_INTERNAL_ERROR);
            Log.log.error("Failed to load new blog post",e);
        }
        return null;
    }
    @RequestMapping("/smlist")
    @ResponseBody
    public QueryCondition smlist(QueryCondition condition) throws BlogException {
        condition.setDefaultPageSize(Constants.SM_LIST);
        QueryCondition page = arcitleService.smList(condition);
        return page;
    }

}
