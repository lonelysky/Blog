package com.sky.blog.admin.controller;

import com.sky.blog.arcitle.entity.Arcitle;
import com.sky.blog.common.exception.BlogException;
import com.sky.blog.common.page.PageQueryBean;
import com.sky.blog.common.resp.Result;
import com.sky.blog.common.resp.ResultUtil;
import com.sky.blog.common.util.MD5Utils;
import com.sky.blog.common.vo.QueryCondition;
import com.sky.blog.user.entity.User;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@Controller
@RequestMapping("admin")
public class adminController {



    @Autowired
    private com.sky.blog.admin.service.adminService adminService;

    /**
     * type  1:post     2:links     3:aboutme
     * @param model
     * @return
     */
    @RequestMapping("/{type}")
    public String postList(Model model, @PathVariable int type) throws BlogException {
        switch(type){
            case 1:
                model.addAttribute("type",1);
                return "admin/index";
            case 2:
                model.addAttribute("type",2);
                return "admin/index";
        }
        throw new BlogException("类型错误，无法解析。请去找前端的麻烦。");
    }
    @RequestMapping("/logout")
    public String logout(HttpSession session){
        session.invalidate();
        return "front/index";
    }
    @RequestMapping("/write")
    public String addPost(){
        return "admin/addpost";
    }
    @RequestMapping("/edit/{id}")
    public String addPost(@PathVariable int id,Model model){
        Arcitle arcitle = adminService.getEditPost(id);
        model.addAttribute("post",arcitle);
        return "admin/addpost";
    }
    @RequestMapping("/aboutme")
    public String aboutMe(Model model, HttpServletRequest request){
        User user = (User) request.getSession().getAttribute("userinfo");
        User s = adminService.getUser(user.getId());
        model.addAttribute("user",s);

        return "admin/aboutme";
    }
    @RequestMapping("/articlelist")
    @ResponseBody
    public QueryCondition postList(QueryCondition condition ) throws BlogException {
        QueryCondition result = adminService.getPostList(condition);
        return result;
    }
    @RequestMapping("/delpost")
    @ResponseBody
    public boolean delPost(int id){
        boolean flag = adminService.delPost(id);
        return flag;
    }
    @RequestMapping("/totalpages")
    @ResponseBody
    public PageQueryBean resultTotalPages(){

        PageQueryBean result = adminService.getTotalPages();
        return result;
    }

    @RequestMapping
    public String adminIndex(){return "admin/index";}

    @RequestMapping("/editaboutme")
    @ResponseBody
    public boolean aboutMe(HttpServletRequest request,String nickname,String context,MultipartFile file ,String username ,String password,String sign) throws BlogException, IOException, NoSuchAlgorithmException {
        User user = (User) request.getSession().getAttribute("userinfo");
        int id = user.getId();
        Boolean flag = adminService.upDateAboutMe(nickname,username,password,context,file,id,sign);
        return flag;
    }

    @RequestMapping("/savepost")
    @ResponseBody
    public boolean save(String title,String context,int id ,String modifypic,MultipartFile file) throws BlogException {

        Boolean flag = adminService.save(title,context,file,id,modifypic);

        return flag;
    }
    @RequestMapping("/uploadpic")
    @ResponseBody
    public Result upload(@RequestParam(value="myFileName")MultipartFile myFileName) throws IOException {

        String []str =  adminService.uploadpic(myFileName);
        return ResultUtil.success(str);
    }

    @RequestMapping("/editlink")
    @ResponseBody
    public boolean linkList(String webname,String weblink,int id) throws BlogException {
       boolean flag = adminService.editLink(webname,weblink,id);
        return  flag;
    }
    @RequestMapping("/dellinks")
    @ResponseBody
    public boolean delLinks(int id) throws BlogException {
        boolean flag = adminService.delLink(id);
        return  flag;
    }
    @RequestMapping("/addlink")
    @ResponseBody
    public boolean addlink(String webname,String weblink) throws BlogException {
        boolean flag = adminService.addlink(webname,weblink);
        return  flag;
    }



}
