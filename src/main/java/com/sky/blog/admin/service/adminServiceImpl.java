package com.sky.blog.admin.service;

import com.sky.blog.arcitle.dao.ArcitleMapper;
import com.sky.blog.arcitle.entity.Arcitle;
import com.sky.blog.common.constants.Constants;
import com.sky.blog.common.exception.BlogException;
import com.sky.blog.common.page.PageQueryBean;
import com.sky.blog.common.util.Log;
import com.sky.blog.common.util.MD5Utils;
import com.sky.blog.common.util.StringUtil;
import com.sky.blog.common.vo.QueryCondition;
import com.sky.blog.links.dao.LinkMapper;
import com.sky.blog.links.entity.Link;
import com.sky.blog.user.dao.UserMapper;
import com.sky.blog.user.entity.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
public class adminServiceImpl implements adminService {

    @Autowired
    private ArcitleMapper arcitleMapper;
    @Autowired
    private com.sky.blog.common.util.cloudStorage cloudStorage;
    @Autowired
    private LinkMapper linkMapper;
    @Autowired
    private UserMapper userMapper;


    @Override
    public boolean save(String title, String context, MultipartFile file, int id, String modifypic) throws BlogException {
        try {
            long i = file.getSize();
            if (StringUtils.isBlank(title) ||StringUtils.isBlank(context)){
                throw new BlogException("文章标题或者内容为空");
            }
            if (id !=0 ){
                update(title,context,file,id,modifypic);
                return true;
            }
            String url = null;
            if(i != 0){
                url = uploadfile(file);
            }
            String summary = StringUtil.delHTMLTag(context);
            String[] sum = summary.split("。");
            Arcitle arcitle = new Arcitle();
            arcitle.setTitle(title);
            arcitle.setContent(context);
            arcitle.setReleasedate(new Date());
            arcitle.setSummary(sum[0]);
            arcitle.setReplyhit(url);
            arcitle.setClickhit(0);
            //暂时不分类
//        arcitle.setTypeid();
            arcitleMapper.insertSelective(arcitle);
            return true;
        } catch (Exception e) {
            Log.log.error("Failed to insert article",e);
        }
        return false;
    }

    @Override
    public String[] uploadpic(MultipartFile myFileName){
        try {
            long i = myFileName.getSize();
            String url = "";
            if (i != 0) {
                String fileName = myFileName.getOriginalFilename();
                // 生成实际存储的真实文件名
                String prefix = String.valueOf(System.currentTimeMillis());
                File excelFile = File.createTempFile(prefix,fileName);
                // MultipartFile to File
                myFileName.transferTo(excelFile);
                url = cloudStorage.upLoad(excelFile);
                excelFile.delete();
                String [] str = {url};
                return str;
            }
        } catch (IOException e) {
            Log.log.error("File read and write failed",e);
        } catch (IllegalStateException e) {
            Log.log.error("Fail to IllegalStateException",e);
        }
        return null;
    }

    @Override
    public QueryCondition getPostList(QueryCondition condition) throws BlogException {
        try {
            condition.setDefaultPageSize(15);
            int count = arcitleMapper.selectByCondition();
            QueryCondition result = new QueryCondition();
            //总条数
            result.setTotalRows(count);
            //当前页
            result.setCurrentPage(condition.getCurrentPage());
            //总页数
            result.setPageSize(condition.getPageSize());
            //数据包装
            List<Arcitle> list = arcitleMapper.selectSmpage(condition);
            result.setItems(list);
            int topage = 0;
            int i = count%Constants.admin_list;
            int j = count/Constants.admin_list;
            if (i==0){
                topage = j;
            }else{
                topage = j+1;
            }
            result.setTotalPage(topage);
            return result;
        } catch (Exception e) {
            Log.log.error("Failed to load article list",e);
            throw new BlogException("加载文章列表失败");
        }
    }

    @Override
    public boolean delPost(int id) {
        try {
            arcitleMapper.deleteByPrimaryKey(id);
            return true;
        }catch (Exception e){
            Log.log.error("Fail to del",e);
            return false;
        }
    }

    @Override
    public PageQueryBean getTotalPages() {
        int count = arcitleMapper.selectByCondition();
        PageQueryBean result = new PageQueryBean();
        int topage = 0;
        int i = count%Constants.admin_list;
        int j = count/Constants.admin_list;
        if (i==0){
            topage = i;
        }else{
            topage = j+1;
        }
        result.setTotalPage(topage);
        return result;
    }

    @Override
    public Arcitle getEditPost(int id) {
        Arcitle arcitle = arcitleMapper.selectByPrimaryKey(id);
        String htmlStr = arcitle.getContent();
        String str = StringUtil.delHTMLTag(htmlStr);
        String finStr = str.replaceAll("&nbsp;","   ");
        arcitle.setContent(finStr);
        return arcitle;
    }

    @Override
    public boolean editLink(String webname, String weblink,int id) throws BlogException {
        if (StringUtils.isBlank(webname)&&StringUtils.isBlank(weblink)){
            throw new BlogException("友链网站名非法，或友链非法");
        }
        Link link = new Link();
        link.setLinkname(webname);
        link.setLinkurl(weblink);
        link.setId(id);
        linkMapper.updateByPrimaryKeySelective(link);
        return true;
    }

    @Override
    public boolean delLink(int id) {
        try {
            linkMapper.deleteByPrimaryKey(id);
            return true;
        } catch (Exception e) {
            Log.log.error("fail del link",e);
            return false;
        }
    }

    @Override
    public Boolean upDateAboutMe(String nickname, String username, String password, String context, MultipartFile file, int id, String sign)  {
        try {
            User user = new User();
            long i = file.getSize();
            String url = null;
            if(i != 0){
               url = uploadfile(file);
             }
             if (password.length()!=0){
                 String newPwd = MD5Utils.encryptPassword(password);
                 user.setPassword(newPwd);
             }
            user.setId(id);
            user.setNickname(nickname);

            user.setUsername(username);
            user.setSign(sign);
            String str = StringUtil.delHTMLTag(context);
            String finStr = str.replaceAll("&nbsp;","   ");
            user.setProfile(finStr);
            user.setImagename(url);
            userMapper.updateByPrimaryKeySelective(user);
            return true;
        } catch (Exception e) {
            Log.log.error("Fail to io",e);
        }
        return false;
    }

    @Override
    public boolean addlink(String webname, String weblink) {
        Link link = new Link();
        link.setLinkname(webname);
        link.setLinkurl(weblink);
        int i = linkMapper.insertSelective(link);
        if (i==0){
            return false;
        }
        return true;

    }

    @Override
    public User getUser(Integer id) {
        User user = userMapper.selectByPrimaryKey(id);
        return user;
    }


    public void update(String title, String context, MultipartFile file, int id, String modifypic) throws IOException, BlogException {
        String url = modifypic;
        long i = file.getSize();
        if(i != 0){
           url = uploadfile(file);
        }
        String summary = StringUtil.delHTMLTag(context);
        String[] sum = summary.split("。");
        Arcitle arcitle = new Arcitle();
        arcitle.setTitle(title);
        arcitle.setContent(context);
        arcitle.setReleasedate(new Date());
        arcitle.setSummary(sum[0]);
        arcitle.setReplyhit(url);
        arcitle.setClickhit(0);
        arcitle.setId(id);
        //暂时不分类
//        arcitle.setTypeid();
        arcitleMapper.updateByPrimaryKeyWithBLOBs(arcitle);
    }

    public String uploadfile(MultipartFile file){
        try {
            String url = null;
            String fileName = file.getOriginalFilename();
            String prefix = String.valueOf(System.currentTimeMillis());
            File excelFile = File.createTempFile(prefix,fileName);
            // MultipartFile to File
            file.transferTo(excelFile);
            url = cloudStorage.upLoad(excelFile);
            excelFile.delete();
            if (url == null){
                throw new BlogException("上传文件失败");
            }
            return url;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BlogException e) {
            e.printStackTrace();
        }
        return null;
    }
}
