package com.sky.blog.admin.service;

import com.sky.blog.arcitle.entity.Arcitle;
import com.sky.blog.common.exception.BlogException;
import com.sky.blog.common.page.PageQueryBean;
import com.sky.blog.common.vo.QueryCondition;
import com.sky.blog.user.entity.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface adminService {


    boolean save(String title, String context, MultipartFile file, int id, String modifypic) throws BlogException;

    String[] uploadpic(MultipartFile myFileName) throws IOException;

    QueryCondition getPostList(QueryCondition condition) throws BlogException;

    boolean delPost(int id);

    PageQueryBean getTotalPages();

    Arcitle getEditPost(int id);


    boolean editLink(String webname, String weblink, int id) throws BlogException;

    boolean delLink(int id);

    Boolean upDateAboutMe(String nickname, String username, String password, String context, MultipartFile file, int id, String sign) throws IOException, BlogException;

    boolean addlink(String webname, String weblink);

    User getUser(Integer id);
}
